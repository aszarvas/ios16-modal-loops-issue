# Problem statement

This example demonstrates a simple modal loop inside an iOS application. This approach worked correctly on devices until iOS 16 and it still works as expected on iOS 16 emulators.

However when testing it on a physical phone running iOS 16.0 or 16.2, as soon as the application enters the modal loop all interaction freezes with the app, and it appears that the messages aren't being delivered to their targets.

I last tested it with Xcode 14.2 on macOS 12.6.2. Regardless of the iOS minimum deployment setting the app will work correctly in the iOS emulators and on a physical phone running iOS 15.5 too. It's only the physical device with 16.0 and 16.2 where the freeze occurs.

# Question

Is there a currently working solution on a physical iOS device to run modal loops? Until iOS 16 the following excerpt from the button press handler of the example was a working approach.

        while (self->counter < 5)
        {
            [[NSRunLoop mainRunLoop] runUntilDate: [NSDate dateWithTimeIntervalSinceNow:0.02]];
        }

What's the reason the app freezes on a physical device? Could the different behavior between the emulator and a physical device an indication of an unintended behavior or bug in iOS 16?
