#import "ViewController.h"

@interface ViewController () {
    int counter;
    bool isModal;
}

@property (weak, nonatomic) IBOutlet UILabel *counterLabel;
@property (weak, nonatomic) IBOutlet UILabel *modalLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self->counter = 0;
    self->isModal = false;
}

- (IBAction)buttonClicked:(id)sender {
    ++(self->counter);
    
    self.counterLabel.text = [NSString stringWithFormat:@"Counter: %d", self->counter];
    
    if (! isModal && self->counter < 5)
    {
        isModal = true;
        
        self.modalLabel.text = @"In modal state";
        
        while (self->counter < 5)
        {
            [[NSRunLoop mainRunLoop] runUntilDate: [NSDate dateWithTimeIntervalSinceNow:0.02]];
        }
        
        self.modalLabel.text = @"Not modal";
        
        isModal = false;
    }
}

@end
